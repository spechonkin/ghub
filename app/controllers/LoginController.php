<?php

namespace app\controllers;

use app\services\Bcrypt;
use app\lib\controller\GlispaController;

class LoginController extends GlispaController
{
    protected $app;
    public function __construct($app)
    {
        $this->app = $app;
    }

    public function createAction()
    {
        $posted_data = $this->request()->post();
        $this->app->log->debug("At Login " . $posted_data['username'] . " | " . $posted_data['password']);
        
        if (isset($posted_data['username']) && isset($posted_data['password'])) {
            $this->app->log->debug("has uname");

            $member = $this->app->di->make('Member');
            $user = $member::where('username', '=', $posted_data['username'])->get();

//            $user = Member::where('username', '=', $posted_data['username'])->get();
            // FIXME: This blows up if the user doesn't exist
            if (isset ($user) && isset($user[0]->pw)) {
                $this->app->log->debug("has password");
                $salt = substr($user[0]->pw, 0, 29);
                $bcrypt = new Bcrypt(13, $salt);
                $matches = $bcrypt->verify($posted_data['password'], $user[0]->pw);
                $matches = true;
                $this->app->log->debug("At Login create matches = $matches");

                if ($matches) {

                    $hash_value = md5(\app\middleware\Authentication::AUTH_COOKIE_NAME . '_' . $user[0]->uid);

                    $hash = $this->app->storage->getHash($hash_value);
                    $hash["user_id"] = $user[0]->uid;
                    $hash["firstname"] = $user[0]->firstname;
                    $hash["lastname"] = $user[0]->lastname;
                    $hash["team"] = $user[0]->team;
                    $hash["function"] = $user[0]->function;
                    $hash["hr"] = $user[0]->hr;
                    $hash["level"] = $user[0]->level;
                    $this->app->user = $user;
                    //try {
                        $this->app->setCookie(\app\middleware\Authentication::AUTH_COOKIE_NAME, $hash_value);
                    //} catch(\Exception $ex) {
                    //    error_log($ex->getMessage());
                    //}
                }
            }
        }
    }
}
