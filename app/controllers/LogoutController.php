<?php
/**
 * Class to remove the cookie of the user.
 *
 * @subpackage Controller
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\controllers;

use app\lib\controller\GlispaController;

class LogoutController extends GlispaController
{
    protected $app;
    public function __construct($app)
    {
        $this->app = $app;
    }

    public function indexAction()
    {
        $cookie = $this->app->getCookie(\app\middleware\Authentication::AUTH_COOKIE_NAME);
        if (!empty($cookie)) {
            $this->app->deleteCookie(\app\middleware\Authentication::AUTH_COOKIE_NAME);
            $this->app->output = array ('status' => 'OK', 'data' => 'User Logged out.');
        } else {
            $this->app->output = array ('status' => 'OK', 'data' => 'No Login session found.');
        }
    }
}
