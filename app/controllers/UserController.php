<?php
namespace app\controllers;

use app\lib\controller\GlispaController;

class UserController extends GlispaController
{
    public function indexAction($id = null)
    {
        $data = $this->request()->get();
        $page = (isset ($data['page'])) ? $data['page'] : 0;

        $member_model = $this->app->di->get('Member');

        $this->app->output = array ('status' => 'OK', 'data' => $member_model::take(30)->offset($page * 30)->get());
    }


    public function showAction($userName)
    {
//        $member_model = $this->app->di->get('Member');
        $member_model = $this->app->di->get('User');
        $this->app->output = array (
            'status' => 'OK',
            'data' => $member_model::where('uname', '=', $userName)->get());
    }

    public function createAction($name)
    {
        $this->app->response()->header('Content-Type', 'application/json');

        $user = $this->app->di->get('User');
        $user->user_name = $name;
        $user->save();

        $this->app->output = array ('status' => 'OK', 'data' => $user::all());
    }
}
