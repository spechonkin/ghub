<?php
namespace app\lib;

/**
 * Class Authentication
 * @package app\lib
 */
class Authentication
{
    /**
     * Check whether the user is authenticated via the id set in the cookie
     *
     * @param \Slim\Slim $app
     * @return bool
     */
    public function isAuthenticated(\Slim\Slim $app)
    {
        $app->log->debug("At Authentication");
        $cookie_value = $app->getEncryptedCookie(\app\middleware\Authentication::AUTH_COOKIE_NAME);

        if (!empty($cookie_value)) {
            $session_value = $app->storage->getHash($cookie_value);
            $app->log->debug("At Authentication 2");
            if (!empty($session_value['user_id'])) {
                $app->setEncryptedCookie(\app\middleware\Authentication::AUTH_COOKIE_NAME, $cookie_value);

                $member_model = $app->di->make('Member');
                $user = $member_model::where('uid', '=', $session_value['user_id'])->get();

                $app->user = $user[0];
                $app->log->debug("Added member");
               
                return true;
            }
        }

        return false;
    }
}
