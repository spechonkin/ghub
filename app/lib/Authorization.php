<?php
namespace app\lib;

/**
 * Authorization module. It looks at the route and calls the method 
 * for the resource. Every resource specific function should store
 * the data it gets when doing the check so the target endpoint
 * doesn't have to execute the same query again
 *
 *
 *
 * @package    ghub
 * @subpackage lib
 * @author     Jon Molin
 */
class Authorization
{
    private $app;

    public function __construct(\SlimController\Slim $app)
    {
        $this->app = $app;
    }


   /**
    * Check if a specific user has the right to watch the specific resource
    * will call the correct function in app/lib/authorization/ResourceName.php->HttpVerb
    *
    * @return boolean true if the user is authorized
    */
    public function isAuthorized()
    {
        $app = $this->app;
        $path = explode(':', $app->router->getCurrentRoute()->getName());
        $path[0] = preg_replace('/^(.*?)Controller$/', '\app\lib\authorization\\\${1}', $path[0]);

        $app->log->debug("app/lib/Authorization will call $path[1] in class $path[0] to check if the user is allowed");

        if (class_exists($path[0])) {
            $resource = new $path[0]($app);
        } else {
            // if an authorization class for the resource hasn't been created
            // will the default one be loaded which denies access for everyone
            // it's not awesome but slightly better than crash and burn
            $resource = new authorization\ResourceAuthorization($app);
        }
        return $resource->$path[1] ();
    }
}
