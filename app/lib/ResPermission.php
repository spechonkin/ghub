<?php
namespace app\lib;

/**
 * Authorization module. It looks at the route and calls the method 
 * for the resource. Every resource specific function should store
 * the data it gets when doing the check so the target endpoint
 * doesn't have to execute the same query again
 *
 *
 *
 * @package    ghub
 * @subpackage lib
 * @author     Jon Molin
 */
class ResPermission
{
    private $app;

    public function __construct(\SlimController\Slim $app)
    {
        $this->app = $app;
    }


   /**
    * Check if a specific user has the right to watch the specific resource
    * will call the correct function in app/lib/authorization/ResourceName.php->HttpVerb
    *
    * @return boolean true if the user is authorized
    */
    public function hasPermission()
    {
        $cookie_value = $this->app->getEncryptedCookie(\app\middleware\Authentication::AUTH_COOKIE_NAME);

        if (!empty($cookie_value)) {
//            var_dump($this->app->storage);
            $session_value = $this->app->storage->getHash($cookie_value);
//            $this->log->debug("At Authentication 2");
//            var_dump($session_value['user_id']);
            if (!empty($session_value['user_id'])) {
//                $this->setEncryptedCookie(\app\middleware\Authentication::AUTH_COOKIE_NAME, $cookie_value);

                $user_model = $this->app->di->make('User');
//                $user = $user_model::where('id', '=', $session_value['user_id'])->get();
                $user = $user_model::find($session_value['user_id']);

                if (empty($user)) {
                    return false;
                }

                $permission = array();

                $user_pug = $user->permissionUserGroup;
                foreach ($user_pug as $pug) {
                    foreach ($pug->permissionsGroup->permissionsRel as $permissions) {
                        $permission[] = $permissions->permissionName;
                    }
                }

                $permission = array_map(function ($item) {
                    return $item->name;
                }, $permission);

                $path = $this->app->router->getCurrentRoute()->getName();

                if (array_search($path, $permission) !== false) {
                    return true;
                }
                return false;

            }
        }

        return false;
    }

    protected function getPermissions($user_id)
    {

    }
}
