<?php
namespace app\lib\authorization;

/**
 * Authorization check for the login resource
 *
 * @package    ghub
 * @subpackage authorization
 * @author     Jon Molin
 */
class Login extends ResourceAuthorization
{
    /**
     * Everyone should be authorized to try to login
     *
     */
    public function create()
    {
        return true;
    }
}
