<?php
namespace app\lib\authorization;

/**
 * Authorization check for the login resource
 *
 * @package    ghub
 * @subpackage authorization
 * @author     Rodrigo De La Garza
 */
class Logout extends ResourceAuthorization
{
    /**
     * Everyone should be authorized to try to logout
     *
     */
    public function index()
    {
        return true;
    }
}
