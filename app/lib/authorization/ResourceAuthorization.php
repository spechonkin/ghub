<?php
namespace app\lib\authorization;

/**
 * Authorization check super class
 *
 * provides a constructor and the default denied to all resources
 * 
 *
 * @package    ghub
 * @subpackage authorization
 * @author     Jon Molin
 */
class ResourceAuthorization
{
    private $app;
    
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * get without parameters
     */
    public function index()
    {
        return false;
    }
    
    /**
     * get with parameters
     */
    public function show()
    {
        return false;
    }

    /*
     * POST or PUT
     */
    public function create()
    {
        return false;
    }
    
    /*
     * POST or PUT
     */
    public function update()
    {
        return false;
    }
    
    /**
     * DELETE
     */
    public function delete()
    {
        return false;
    }
}
