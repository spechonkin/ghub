<?php
namespace app\lib\authorization;

/**
 * Authorization check for the user resource
 *
 * @package    ghub
 * @subpackage authorization
 * @author     Jon Molin
 */
class User extends ResourceAuthorization
{
    /**
     * Who is allowed to get a listing of all employees?
     */
    public function index()
    {
        return true;
    }
    
    /**
     * Who is allowed to check a specific employee?
     */
    public function show()
    {
        return true;
    }
}
