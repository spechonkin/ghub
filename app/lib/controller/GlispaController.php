<?php
namespace app\lib\controller;

use SlimController\SlimController;

/**
 * Class GlispaController
 * @package app\lib\controller
 */
class GlispaController extends SlimController
{
    /**
     * @param \Slim\Slim $app
     */
    public function __construct(&$app)
    {
        parent::__construct($app);
    }
}
