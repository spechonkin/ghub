<?php

namespace app\lib\exceptions;

/**
* HTTP 403 Exception
*/
class HttpNotExistingException extends \RuntimeException
{

/**
 * Public constructor
 *
 * @param string $message Exception message
 * @param int $code Exception code
 * @param \Exception $previous Previous exception
 */
    public function __construct(
        $message = "This resource doesn't exist",
        $code = 404,
        \Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
