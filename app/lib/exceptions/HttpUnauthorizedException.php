<?php

namespace app\lib\exceptions;

/**
* HTTP 401 Exception
*/
class HttpUnauthorizedException extends \RuntimeException
{

/**
 * Public constructor
 *
 * @param string $message Exception message
 * @param int $code Exception code
 * @param \Exception $previous Previous exception
 */
    public function __construct(
        $message = 'You are not authorized to access this resource',
        $code = 401,
        \Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
