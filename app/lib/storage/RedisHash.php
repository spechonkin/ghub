<?php
namespace app\lib\storage;

use Predis\Client;

/**
 * Class RedisHash
 * @package app\lib\storage
 */
class RedisHash implements \ArrayAccess, \IteratorAggregate
{
    const CONTROL_KEY_POSTFIX = '__expiration';

    /**
     * @var \Predis\Client
     */
    private $client;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $expiration;

    /**
     * @var bool
     */
    private $renew_on_access;

    /**
     * If you use $expiration you might want to use $renew_on_access to keep your hash alive while you are using it.
     *
     * @param \Predis\Client $client
     * @param string $name
     * @param int $expiration
     * @param bool $renew_on_access
     * @throws \InvalidArgumentException
     */
    public function __construct(
        Client $client,
        $name,
        $expiration = RedisStorage::DEFAULT_EXPIRATION_TIME,
        $renew_on_access = true
    ) {
        if (empty($name)) {
            throw new \InvalidArgumentException("The hash has to be named!");
        }

        $this->client = $client;
        $this->name = $name;
        $this->expiration = $expiration > 0 ? (int)$expiration : 0;
        $this->renew_on_access = ($renew_on_access === true);

        if ($this->expiration > 0) {
            $this->client->setex($this->name.self::CONTROL_KEY_POSTFIX, $this->expiration, $this->expiration);
        }
    }

    /**
     * Expire the hash via another key, since hashes in redis don't have a lifetime.
     * This will be called on every read & write access to the hash.
     */
    private function expire()
    {
        $controlKeyName = $this->name.self::CONTROL_KEY_POSTFIX;

        if ($this->expiration > 0) {
            // If the control key doesn't exist, delete the hash
            if (!$this->client->exists($controlKeyName)) {
                $this->client->del($this->name);
            }

            // Renew the control key
            if ($this->renew_on_access) {
                $this->client->setex($controlKeyName, $this->expiration, $this->expiration);
            }
        }
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return \Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     */
    public function getIterator()
    {
        $this->expire();
        return new \ArrayIterator($this->client->hgetall($this->name));
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset)
    {
        $this->expire();

        return $this->client->hexists($this->name, $offset);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset)
    {
        $this->expire();

        return $this->client->hget($this->name, $offset);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->expire();

        $this->client->hset($this->name, $offset, $value);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     */
    public function offsetUnset($offset)
    {
        $this->expire();

        $this->client->hdel($this->name, $offset);
    }
}
