<?php
namespace app\lib\storage;

use Predis\Client;

/**
 * Class RedisStorage
 * @package app\lib\storage
 */
class RedisStorage
{
    const DEFAULT_EXPIRATION_TIME = 3600;

    /**
     * @var \Predis\Client
     */
    private $client;

    /**
     * @param \Predis\Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Get a hash/array
     *
     * @param string $name
     * @param int $expiration
     * @param bool $renew_on_access
     * @return RedisHash
     */
    public function getHash(
        $name,
        $expiration = self::DEFAULT_EXPIRATION_TIME,
        $renew_on_access = true
    ) {
        return new RedisHash($this->client, $name, $expiration, $renew_on_access);
    }

    /**
     * Set a key's value
     *
     * @param string $name
     * @param mixed $value
     * @param int $expiration
     */
    public function set(
        $name,
        $value,
        $expiration = self::DEFAULT_EXPIRATION_TIME
    ) {
        $this->client->setex($name, $expiration, $value);
    }

    /**
     * Get a key's value
     *
     * @param string $name
     * @return mixed
     */
    public function get($name)
    {
        return $this->client->get($name);
    }

    /**
     * Delete a key
     *
     * @param string $name
     */
    public function delete($name)
    {
        $this->client->del($name);
    }
}
