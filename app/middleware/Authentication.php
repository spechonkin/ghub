<?php

namespace app\middleware;

use \app\lib\exceptions\HttpUnauthorizedException;
use \Slim\Middleware;

/**
 * Class Authentication
 * @package app\middleware
 */
class Authentication extends Middleware
{
    const AUTH_COOKIE_NAME = 'ghid';

    /**
     * Add an authentication hook
     */
    public function call()
    {
        $app = $this->app;

        $isAuthenticated = function () use ($app) {
            $auth = new \app\lib\Authentication($app);

            if (!$auth->isAuthenticated($app)) {
                throw new HttpUnauthorizedException();
            }
        };

        $app->hook('slim.before.dispatch', $isAuthenticated);
        $this->next->call();
    }
}
