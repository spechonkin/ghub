<?php
namespace app\middleware;

/**
 * Middleware injecting authorization check at slim.before.dispatch
 */
class Authorization extends \Slim\Middleware
{
    public function call()
    {
        $app = $this->app;

        $isAuthorized = function () use ($app) {
            $auth = new \app\lib\Authorization($app);
            if (!$auth->isAuthorized()) {
                throw new \app\lib\exceptions\HttpForbiddenException();
                $app->status(403);
                $app->output = array ('status' => 'Forbidden');
            }

        };
        $app->hook('slim.before.dispatch', $isAuthorized);
        $this->next->call();
    }
}
