<?php
namespace app\middleware;

/** 
 * Do content negotiation for the output
 *
 */
class ContentNegotiation extends \Slim\Middleware
{
    public static $priorities = array('application/json'); // , 'application/csv', 'application/xml');

    /**
     * call is the function Slim calls middleware
     * just registers a function to be called after 
     * all routers are done
     *
     */
    public function call()
    {
        $app = $this->app;
        $app->hook(
            'slim.after.router',
            function () {
                ContentNegotiation::negotiateContent();
            }
        );
        $this->next->call();
    }


    /**
     * This function is being called after the request finishes and 
     * will determine what output type to use from $priorities
     *
     * 
     * FIXME: Currently only json works properly
     */
    public function negotiateContent()
    {
        $app =  \Slim\Slim::getInstance();
        $acceptHeader = $app->request()->headers('Accept');
        $negotiator   = new \Negotiation\FormatNegotiator();
        $priorities   = array('application/json', 'application/csv', 'application/xml');
        $priorities   = array('application/json');
        $format       = $negotiator->getBest($acceptHeader, $priorities)->getValue();
        error_log("Format set to $format from $acceptHeader\n");
        
        $app->response()->header('Content-Type', $format);
        $output = $app->output;

        if ($format == 'application/xml') {
            $xml = new \SimpleXMLElement('<root/>');
            ContentNegotiation::addnode($xml, $app->output);
            echo $xml->asXML();
        } elseif ($format == 'application/csv') {
            $output = fopen("php://output", 'w') or die("Can't open php://output");
            fputcsv($output, $app->output);
            fclose($output);
        } else {
            echo json_encode($app->output);
        }
    }
    
    public static function addnode($xml, $arr = array ())
    {
        $app =  \Slim\Slim::getInstance();
        foreach ($arr as $key => $value) {
            if (gettype($value) == "array") {
                $child = $xml->addChild($key);
                $app->log->debug("addnode $key");
                ContentNegotiation::addnode($child, $value);
            } else {
                $xml->addChild($key, $value);
            }
        }
    }
}
