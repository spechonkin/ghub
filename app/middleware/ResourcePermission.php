<?php
namespace app\middleware;

/**
 * Middleware injecting authorization check at slim.before.dispatch
 */
class ResourcePermission extends \Slim\Middleware
{
    public function call()
    {
        $app = $this->app;

        $hasPermission = function () use ($app) {
            $auth = new \app\lib\ResPermission($app);
            if (!$auth->hasPermission()) {
                throw new \app\lib\exceptions\HttpForbiddenException();
//                $app->status(403);
//                $app->output = array ('status' => 'Forbidden');
            }

        };
        $app->hook('slim.before.dispatch', $hasPermission);
        $this->next->call();
    }
}
