<?php
/**
 * A class for accessing and modifying Users.
 *
 *
 * @subpackage models
 */

namespace app\models;

class Member extends \Illuminate\Database\Eloquent\Model
{
    protected $connection = 'wk3';
    protected $table = 'members';
    protected $primaryKey = 'uid';
    public $timestamps = false;
}
