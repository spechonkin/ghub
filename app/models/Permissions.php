<?php
/**
 * A class for accessing and modifying Users.
 *
 *
 * @subpackage models
 */

namespace app\models;

class Permissions extends \Illuminate\Database\Eloquent\Model
{
    protected $connection = 'slimtest';
    protected $table = 'permissions';
    protected $primaryKey = 'id';
    public $timestamps = true;
}
