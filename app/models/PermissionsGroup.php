<?php
/**
 * A class for accessing and modifying Users.
 *
 *
 * @subpackage models
 */

namespace app\models;

class PermissionsGroup extends \Illuminate\Database\Eloquent\Model
{
    protected $connection = 'slimtest';
    protected $table = 'permissions_group';
    protected $primaryKey = 'id';
    public $timestamps = true;

    /**
     * Return the relation between the group and the permission
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permissionsRel()
    {
        return $this->hasMany('app\models\PermissionsGroupRel', 'group_id', 'id');
    }
}
