<?php
/**
 * A class for accessing and modifying Users.
 *
 *
 * @subpackage models
 */

namespace app\models;

class PermissionsGroupRel extends \Illuminate\Database\Eloquent\Model
{
    protected $connection = 'slimtest';
    protected $table = 'permissions_group_rel';
    protected $primaryKey = 'id';
    public $timestamps = true;

    /**
     * Return the name of the permission
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function permissionName()
    {
        return $this->hasOne('app\models\Permissions', 'id', 'permission_id');
    }
}
