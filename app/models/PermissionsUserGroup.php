<?php
/**
 * A class for accessing and modifying Users.
 *
 *
 * @subpackage models
 */

namespace app\models;

class PermissionsUserGroup extends \Illuminate\Database\Eloquent\Model
{
    protected $connection = 'slimtest';
    protected $table = 'permissions_user_group';
    protected $primaryKey = 'id';
    public $timestamps = true;

    /**
     * Return the user group
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function permissionsGroup()
    {
        return $this->belongsTo('app\models\PermissionsGroup', 'group_id', 'id');
    }
}
