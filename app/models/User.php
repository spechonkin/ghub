<?php
/**
 * A class for accessing and modifying Users.
 *
 *
 * @subpackage models
 */

namespace app\models;

class User extends \Illuminate\Database\Eloquent\Model
{
    protected $connection = 'slimtest';
    protected $table = 'user';
    protected $primaryKey = 'id';
    public $timestamps = true;

    /**
     * Return the relation between the user and a group
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permissionUserGroup()
    {
        return $this->hasMany('app\models\PermissionsUserGroup', 'user_id', 'id');
    }
}
