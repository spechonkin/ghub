Unit Testing
=========


---
Setting up the dependencies
------------


Build the package as described in https://bitbucket.org/jonmolin/php-testing-pkg and install the package.
```sh
sudo apt-get install ant
```

-----
Run the tests:
-----
There are two ways of running the tests, quick (PHPUNIT), complete (PHPUNIT, Code Coverage, Mess detector, etc).

1.- Quick 
---
Under your working directory at the project root level, run.

```sh
user@ThinkPad:/var/www/ghub$ ant test 
```
this test will execute all tes cases under the tests->cases folder, and will return a report similar to the next one in case there are no errors detected.
```sh
user@ThinkPad:/var/www/ghub$ ant test
Buildfile: /var/www/ghub/build.xml

clean:
   [delete] Deleting directory /var/www/ghub/test_results
   [delete] Deleting: /var/www/ghub/cache.properties
   [delete] Deleting: /var/www/ghub/app/test/conf/phpunit.xml

connection-exist:

prepare-test-connection-backup:
     [copy] Copying 1 file to /var/www/ghub/config

prepare-test-connection:
     [copy] Copying 1 file to /var/www/ghub/config

prepare:
    [mkdir] Created dir: /var/www/ghub/test_results/coverage
    [mkdir] Created dir: /var/www/ghub/test_results/logs
    [mkdir] Created dir: /var/www/ghub/test_results/pdepend
    [mkdir] Created dir: /var/www/ghub/test_results/phpdox
     [copy] Copying 1 file to /var/www/ghub/app/test/conf
     [exec] Loading composer repositories with package information
     [exec] Updating dependencies (including require-dev)
     [exec] Nothing to install or update
     [exec] Generating autoload files

phpunit-quick-test:
     [exec] PHPUnit 4.1.6 by Sebastian Bergmann.
     [exec]
     [exec] Configuration read from /var/www/ghub/app/tests/conf/phpunit.xml
     [exec]
     [exec] ......................................
     [exec]
     [exec] Time: 3.63 seconds, Memory: 11.25Mb
     [exec]
     [exec] OK (38 tests, 46 assertions)

restore-connection:
     [move] Moving 1 file to /var/www/ghub/config

restore:

test:

BUILD SUCCESSFUL
Total time: 35 seconds
```


2.- Complete
---
Under your working directory at the project root level, run.

```sh
user@ThinkPad:/var/www/ghub$ ant build
```
This test will execute all test cases under the tests->cases folder, plus will perform a series of reports related to the code, a normal output of this command is the next one:
```sh
user@ThinkPad:/var/www/ghub$ ant build
Buildfile: /var/www/ghub/build.xml

clean:
   [delete] Deleting directory /var/www/ghub/test_results
   [delete] Deleting: /var/www/ghub/app/test/conf/phpunit.xml

connection-exist:

prepare-test-connection-backup:
     [copy] Copying 1 file to /var/www/ghub/config

prepare-test-connection:
     [copy] Copying 1 file to /var/www/ghub/config

prepare:
    [mkdir] Created dir: /var/www/ghub/test_results/coverage
    [mkdir] Created dir: /var/www/ghub/test_results/logs
    [mkdir] Created dir: /var/www/ghub/test_results/pdepend
    [mkdir] Created dir: /var/www/ghub/test_results/phpdox
     [copy] Copying 1 file to /var/www/ghub/app/test/conf
     [exec] Loading composer repositories with package information
     [exec] Updating dependencies (including require-dev)
     [exec] Nothing to install or update
     [exec] Generating autoload files

lint:
    [apply] No syntax errors detected in /var/www/ghub/app/routes/user.php
    [apply] No syntax errors detected in /var/www/ghub/app/models/Member.php
    [apply] No syntax errors detected in /var/www/ghub/app/models/User.php
    [apply] No syntax errors detected in /var/www/ghub/app/test/bootstrap.php
    [apply] No syntax errors detected in /var/www/ghub/app/test/cases/config/DependenciesTest.php
    [apply] No syntax errors detected in /var/www/ghub/app/test/cases/config/ExceptionsTest.php
    [apply] No syntax errors detected in /var/www/ghub/app/test/cases/connectionTest.php
    [apply] No syntax errors detected in /var/www/ghub/app/test/cases/controllers/LoginControllerTest.php
    [apply] No syntax errors detected in /var/www/ghub/app/test/cases/controllers/UserControllerTest.php
    [apply] No syntax errors detected in /var/www/ghub/app/test/cases/lib/AuthenticationTest.php
    [apply] No syntax errors detected in /var/www/ghub/app/test/cases/lib/AuthorizationTest.php
    [apply] No syntax errors detected in /var/www/ghub/app/test/cases/lib/authorization/ResourceAuthorizationTest.php
    [apply] No syntax errors detected in /var/www/ghub/app/test/cases/lib/storage/RedisHashTest.php
    [apply] No syntax errors detected in /var/www/ghub/app/test/cases/lib/storage/RedisStorageTest.php
    [apply] No syntax errors detected in /var/www/ghub/app/test/cases/middleware/AuthenticationTest.php
    [apply] No syntax errors detected in /var/www/ghub/app/test/cases/middleware/AuthorizationTest.php
    [apply] No syntax errors detected in /var/www/ghub/app/test/cases/services/BcryptTest.php
    [apply] No syntax errors detected in /var/www/ghub/app/test/conf/FixtureTestCase.php

phploc:
     [exec] phploc 2.0.6 by Sebastian Bergmann.
     [exec]
     [exec] Directories                                          9
     [exec] Files                                               21
     [exec]
     [exec] Size
     [exec]   Lines of Code (LOC)                              927
     [exec]   Comment Lines of Code (CLOC)                     296 (31.93%)
     [exec]   Non-Comment Lines of Code (NCLOC)                631 (68.07%)
     [exec]   Logical Lines of Code (LLOC)                     237 (25.57%)
     [exec]     Classes                                        194 (81.86%)
     [exec]       Average Class Length                           9
     [exec]       Average Method Length                          4
     [exec]     Functions                                       10 (4.22%)
     [exec]       Average Function Length                        1
     [exec]     Not in classes or functions                     33 (13.92%)
     [exec]
     [exec] Complexity
     [exec]   Cyclomatic Complexity / LLOC                    0.16
     [exec]   Cyclomatic Complexity / Number of Methods       1.84
     [exec]
     [exec] Dependencies
     [exec]   Global Accesses                                    0
     [exec]     Global Constants                                 0 (0.00%)
     [exec]     Global Variables                                 0 (0.00%)
     [exec]     Super-Global Variables                           0 (0.00%)
     [exec]   Attribute Accesses                               111
     [exec]     Non-Static                                     111 (100.00%)
     [exec]     Static                                           0 (0.00%)
     [exec]   Method Calls                                      89
     [exec]     Non-Static                                      74 (83.15%)
     [exec]     Static                                          15 (16.85%)
     [exec]
     [exec] Structure
     [exec]   Namespaces                                         9
     [exec]   Interfaces                                         0
     [exec]   Traits                                             0
     [exec]   Classes                                           20
     [exec]     Abstract Classes                                 0 (0.00%)
     [exec]     Concrete Classes                                20 (100.00%)
     [exec]   Methods                                           44
     [exec]     Scope
     [exec]       Non-Static Methods                            43 (97.73%)
     [exec]       Static Methods                                 1 (2.27%)
     [exec]     Visibility
     [exec]       Public Methods                                40 (90.91%)
     [exec]       Non-Public Methods                             4 (9.09%)
     [exec]   Functions                                          7
     [exec]     Named Functions                                  1 (14.29%)
     [exec]     Anonymous Functions                              6 (85.71%)
     [exec]   Constants                                          3
     [exec]     Global Constants                                 0 (0.00%)
     [exec]     Class Constants                                  3 (100.00%)

pdepend:
     [exec] PDepend 2.0.0
     [exec]
     [exec] Parsing source files:
     [exec] ....................................                            36
     [exec]
     [exec] Calculating Dependency metrics:
     [exec] .......                                                        149
     [exec]
     [exec] Calculating Coupling metrics:
     [exec] ..........                                                     215
     [exec]
     [exec] Calculating Cyclomatic Complexity metrics:
     [exec] ..........                                                     215
     [exec]
     [exec] Calculating Inheritance metrics:
     [exec] ..                                                              59
     [exec]
     [exec] Calculating Node Count metrics:
     [exec] .......                                                        151
     [exec]
     [exec] Calculating Node Loc metrics:
     [exec] .........                                                      188
     [exec]
     [exec] Generating pdepend log files, this may take a moment.
     [exec]
     [exec] Time: 00:02; Memory: 14.50Mb

phpmd-ci:
     [exec] Result: 2

phpcs-ci:
     [exec] Result: 1

phpcpd:
     [exec] phpcpd 2.0.1 by Sebastian Bergmann.
     [exec]
     [exec] 0.00% duplicated lines out of 2237 total lines of code.
     [exec]
     [exec] Time: 238 ms, Memory: 4.00Mb

phpunit:
     [exec] PHPUnit 4.1.6 by Sebastian Bergmann.
     [exec]
     [exec] Configuration read from /var/www/ghub/app/test/conf/phpunit.xml
     [exec]
     [exec] ......................................
     [exec]
     [exec] Time: 9.02 seconds, Memory: 16.75Mb
     [exec]
     [exec] OK (38 tests, 46 assertions)
     [exec]
     [exec] Generating code coverage report in Clover XML format ... done
     [exec]
     [exec] Generating code coverage report in HTML format ... done
     [exec]
     [exec] Generating code coverage report in PHPUnit XML format ... done

phpdox:
     [exec] phpDox 681a84b-dirty - Copyright (C) 2010 - 2014 by Arne Blankerts
     [exec]
     [exec] [09.09.2014 - 14:09:03] Using config file '/var/www/ghub/app/test/conf/phpdox.xml'
     [exec] [09.09.2014 - 14:09:03] Registered enricher 'build'
     [exec] [09.09.2014 - 14:09:03] Registered enricher 'git'
     [exec] [09.09.2014 - 14:09:03] Registered enricher 'checkstyle'
     [exec] [09.09.2014 - 14:09:03] Registered enricher 'pmd'
     [exec] [09.09.2014 - 14:09:03] Registered enricher 'phpunit'
     [exec] [09.09.2014 - 14:09:03] Registered enricher 'phploc'
     [exec] [09.09.2014 - 14:09:03] Registered output engine 'xml'
     [exec] [09.09.2014 - 14:09:03] Registered output engine 'html'
     [exec] [09.09.2014 - 14:09:03] Starting to process project 'Example'
     [exec] [09.09.2014 - 14:09:03] Starting collector
     [exec] [09.09.2014 - 14:09:03] Scanning directory 'app' for files to process
     [exec]
     [exec] ..................................                	[34]
     [exec]
     [exec] [09.09.2014 - 14:09:05] Saving results to directory 'test_results/api/xml'
     [exec] [09.09.2014 - 14:09:05] Resolving inheritance
     [exec]
     [exec] ...............................                   	[31]
     [exec]
     [exec] [09.09.2014 - 14:09:05] The following unit(s) had missing dependencies during inheritance resolution:
     [exec] [09.09.2014 - 14:09:05]  - app\lib\controller\GlispaController (missing SlimController\SlimController)
     [exec] [09.09.2014 - 14:09:05]  - app\tests\cases\lib\AuthorizationTest (missing PHPUnit_Framework_TestCase)
     [exec] [09.09.2014 - 14:09:05]  - app\tests\cases\lib\authorization\ResourceAuthorizationTest (missing PHPUnit_Framework_TestCase)
     [exec] [09.09.2014 - 14:09:05]  - app\tests\cases\lib\storage\RedisStorageTest (missing PHPUnit_Framework_TestCase)
     [exec] [09.09.2014 - 14:09:05]  - app\tests\cases\lib\storage\RedisHashTest (missing PHPUnit_Framework_TestCase)
     [exec] [09.09.2014 - 14:09:05]  - app\tests\cases\lib\AuthenticationTest (missing PHPUnit_Extensions_Database_TestCase)
     [exec] [09.09.2014 - 14:09:05]  - app\tests\cases\middleware\AuthorizationTest (missing PHPUnit_Framework_TestCase)
     [exec] [09.09.2014 - 14:09:05]  - app\tests\cases\middleware\AuthenticationTest (missing PHPUnit_Framework_TestCase)
     [exec] [09.09.2014 - 14:09:05]  - app\tests\cases\connectionTest (missing PHPUnit_Framework_TestCase)
     [exec] [09.09.2014 - 14:09:05]  - app\tests\cases\services\BcryptTest (missing PHPUnit_Framework_TestCase)
     [exec] [09.09.2014 - 14:09:05]  - app\tests\cases\controllers\LoginControllerTest (missing PHPUnit_Extensions_Database_TestCase)
     [exec] [09.09.2014 - 14:09:05]  - app\tests\cases\controllers\UserControllerTest (missing PHPUnit_Extensions_Database_TestCase)
     [exec] [09.09.2014 - 14:09:05]  - app\tests\conf\FixtureTestCase (missing PHPUnit_Extensions_Database_TestCase)
     [exec] [09.09.2014 - 14:09:05]  - app\middleware\Authorization (missing Slim\Middleware)
     [exec] [09.09.2014 - 14:09:05]  - app\middleware\Authentication (missing Slim\Middleware)
     [exec] [09.09.2014 - 14:09:05]  - app\middleware\ContentNegotiation (missing Slim\Middleware)
     [exec] [09.09.2014 - 14:09:05]  - app\models\User (missing Illuminate\Database\Eloquent\Model)
     [exec] [09.09.2014 - 14:09:05]  - app\models\Member (missing Illuminate\Database\Eloquent\Model)
     [exec] [09.09.2014 - 14:09:05]  - app\controllers\UserController (missing SlimController\SlimController)
     [exec] [09.09.2014 - 14:09:05]  - app\controllers\LoginController (missing SlimController\SlimController)
     [exec] [09.09.2014 - 14:09:05] Collector process completed
     [exec]
     [exec] [09.09.2014 - 14:09:05] Starting generator
     [exec] [09.09.2014 - 14:09:05] Loading enrichers
     [exec] [09.09.2014 - 14:09:05] Enricher PHPLoc xml initialized successfully
     [exec] [09.09.2014 - 14:09:05] Enricher GIT information initialized successfully
     [exec] [09.09.2014 - 14:09:05] Enricher CheckStyle XML initialized successfully
     [exec] [09.09.2014 - 14:09:05] Enricher PHPMessDetector XML initialized successfully
     [exec] [09.09.2014 - 14:09:05] Enricher PHPUnit Coverage XML initialized successfully
     [exec] [09.09.2014 - 14:09:05] Starting event loop.
     [exec]
     [exec] ..................................................	[50]
     [exec] ..................................................	[100]
     [exec] ..................................................	[150]
     [exec] ..................................................	[200]
     [exec] ..................................................	[250]
     [exec] ..................................................	[300]
     [exec] ..................................................	[350]
     [exec] ...........                                       	[361]
     [exec]
     [exec] [09.09.2014 - 14:09:07] Generator process completed
     [exec] [09.09.2014 - 14:09:07] Processing project 'Example' completed.
     [exec]
     [exec]
     [exec] Time: 3.63 seconds, Memory: 7.50Mb
     [exec]

restore-connection:
     [move] Moving 1 file to /var/www/ghub/config

restore:

build:

BUILD SUCCESSFUL
Total time: 1 minute 7 seconds
```