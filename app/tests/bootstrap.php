<?php
set_include_path(dirname(__FILE__) . '/../' . PATH_SEPARATOR . get_include_path());

define('APP_PATH', dirname(dirname(__DIR__)));
require_once APP_PATH . '/vendor/autoload.php';


//Register Models and Controllers autoloader
function customAutoLoader($class)
{
    $file = APP_PATH.'/' . str_replace('\\', '/', $class) . '.php';

    if (file_exists($file)) {
        require $file;
    } else {
        return;
    }
}
spl_autoload_register('customAutoLoader');
