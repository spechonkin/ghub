<?php
/**
 * testing class configuration
 *
 *
 * @subpackage Config
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\tests\cases\config;

class DependenciesTest extends \PHPUnit_Framework_TestCase
{
    protected $app = null;
    /**
     * Runs one before each test
     * Set up the necessary classes
     */
    public function setUp()
    {
        require_once 'config/Dependencies.php';
        require_once 'config/Exceptions.php';

        $this->app = new \SlimController\Slim(
            array(
                'debug'         => false,
                'log.enabled'   => false,
                'cookies.secret_key' => 'wk4',
                'cookies.lifetime' => '1 hour',
                'controller.class_prefix'    => '\\app\\controllers',
                'models.class_prefix'        => '\\app\\models',
            )
        );
    }

    /**
     * Function testing the dependency injector for a New model (User)
     */
    public function testValidDependencyInjectionOfNewUserModel()
    {
        $dep = new \config\Dependencies($this->app);
        $this->app->di = $dep;

        $this->assertInstanceOf('app\models\User', $this->app->di->make('User'));
    }

    /**
     * Function testing the dependency injector for a New model (User) with a different set ame
     */
    public function testValidDependencyInjectionOfNewUserModelWithName()
    {
        $dep = new \config\Dependencies($this->app);
        $this->app->di = $dep;

        $this->assertInstanceOf('app\models\User', $this->app->di->make('User', 'UserTestMake'));
    }

    /**
     * Function testing the dependency injector for a model (User)
     */
    public function testValidDependencyInjectionOfUserModel()
    {
        $dep = new \config\Dependencies($this->app);
        $this->app->di = $dep;

        $this->assertInstanceOf('app\models\User', $this->app->di->get('User'));
    }

    /**
     * Function testing the dependency injector for a model (User) with a different set ame
     */
    public function testValidDependencyInjectionOfUserModelWithName()
    {
        $dep = new \config\Dependencies($this->app);
        $this->app->di = $dep;

        $this->assertInstanceOf('app\models\User', $this->app->di->get('User', 'UserTestGet'));
    }

    /**
     * Function testing the dependency injector for a model (User)
     */
    public function testValidReturnOfStandardClass()
    {
        $dep = new \config\Dependencies($this->app);
        $this->app->di = $dep;

        $this->assertInstanceOf('\stdClass', $this->app->di->make('UserOther'));
    }

    /**
     * Function testing the dependency injector for a model (User)
     */
    public function testInValidReturnOfStandardClass()
    {
        $dep = new \config\Dependencies($this->app);
        $this->app->di = $dep;

        $this->assertNotInstanceOf('\stdClass', $this->app->di->make('User'));
    }

    /**
     * Function to the the DI method does not have class
     */
    public function testInstanceOfClassNotDefined()
    {
        $dep = new \config\Dependencies($this->app);
        $this->app->di = $dep;

        $this->assertFalse($this->app->di->has('User_test'));
    }

    /**
     * Function to the the DI method does have class
     */
    public function testInstanceOfClassDefined()
    {
        $dep = new \config\Dependencies($this->app);
        $this->app->di = $dep;

        $this->app->di->get('User_tes');

        $this->assertTrue($this->app->di->has('User_tes'));
    }
}
