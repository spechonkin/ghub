<?php
/**
 * Exceptions Test class
 * Tes the exception class call.
 *
 *
 * @subpackage Controller
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\tests\cases\config;


class ExceptionsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test Exception 401
     */
    public function testValidExceptionClassCall401()
    {
        $app = new \Slim\Slim(
            array(
                'debug'         => false,
                'log.enabled'   => false,
            )
        );

        \Slim\Environment::mock(array(
            'PATH_INFO' => '/bar', //<-- Virtual
        ));

        $exx = new \config\Exceptions($app);
        $exx->addExceptionHandlers();

        $app->get('/bar', function () {
            throw new \Exception('Message test Exception', 401);
        });

        $output = array();
        ob_start();
        $app->run();

        ob_get_clean();

        $output = $app->output;

        ob_end_flush();

        $this->assertEquals(401, $output['statusCode']);
        $this->assertEquals('Message test Exception', $output['message']);
    }

    /**
     * Test Exception 403
     */
    public function testValidExceptionClassCall403()
    {
        $app = new \Slim\Slim(
            array(
                'debug'         => false,
                'log.enabled'   => false,
            )
        );

        \Slim\Environment::mock(array(
            'PATH_INFO' => '/bar', //<-- Virtual
        ));

        $exx = new \config\Exceptions($app);
        $exx->addExceptionHandlers();

        $app->get('/bar', function () {
            throw new \Exception('Message test Exception', 403);
        });

        $output = array();
        ob_start();
        $app->run();

        ob_get_clean();

        $output = $app->output;

        ob_end_flush();

        $this->assertEquals(403, $output['statusCode']);
        $this->assertEquals('Message test Exception', $output['message']);
    }

    /**
     * Test Exception 404
     */
    public function testValidExceptionClassCall404()
    {
        $app = new \Slim\Slim(
            array(
                'debug'         => false,
                'log.enabled'   => false,
            )
        );

        \Slim\Environment::mock(array(
            'PATH_INFO' => '/bar', //<-- Virtual
        ));

        $exx = new \config\Exceptions($app);
        $exx->addExceptionHandlers();

        $app->get('/bar', function () {
            throw new \Exception('Message test Exception', 404);
        });

        $output = array();
        ob_start();
        $app->run();

        ob_get_clean();

        $output = $app->output;

        ob_end_flush();

        $this->assertEquals(404, $output['statusCode']);
        $this->assertEquals('Message test Exception', $output['message']);
    }

    /**
     * Test Default Exception call
     */
    public function testValidExceptionClassCallDefault()
    {
        $app = new \Slim\Slim(
            array(
                'debug'         => false,
                'log.enabled'   => false,
            )
        );

        \Slim\Environment::mock(array(
            'PATH_INFO' => '/bar', //<-- Virtual
        ));

        $exx = new \config\Exceptions($app);
        $exx->addExceptionHandlers();

        $app->get('/bar', function () {
            throw new \Exception('Message test Exception', 666);
        });

        $output = array();
        ob_start();
        $app->run();

        ob_get_clean();

        $output = $app->output;

        ob_end_flush();

        $this->assertEquals('Message test Exception', $output['message']);
    }
}
