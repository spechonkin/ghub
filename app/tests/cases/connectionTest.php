<?php
/**
 * testing class for existence of connection class
 *
 *
 * @subpackage Controller
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\tests\cases;

class ConnectionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test if connection exist by validating an exception.
     */
    public function testValidConnectionToDatabase()
    {
        $this->assertFileExists(dirname(dirname(dirname(__DIR__))) . '/config/Connection.php');
    }
}
