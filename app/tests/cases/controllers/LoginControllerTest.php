<?php
/**
 * testing class for Login Controller
 *
 * Extending FixturesConnector Class
 *
 *
 * @subpackage Config
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\tests\cases\controllers;

use app\controllers\LoginController;
use app\lib\storage\RedisStorage;
use app\middleware\Authentication;
use app\tests\conf\FixtureTestCase;
use app\models\Member;

class LoginControllerTest extends FixtureTestCase
{
    protected $app = null;
    protected $connection_resolver = null;
    protected $connection = null;

    /**
     * Runs one before each test
     * Set up the necessary SLIM instance
     * and fixtures XML to load
     */
    public function setUp()
    {
        $this->app = new \SlimController\Slim(
            array(
                'debug'         => false,
                'log.enabled'   => false,
                'cookies.secret_key' => 'wk4',
                'cookies.lifetime' => '1 hour',
                'controller.class_prefix'    => '\\app\\controllers',
                'models.class_prefix'        => '\\app\\models',
            )
        );

        $instanceHandler = new \config\Dependencies($this->app);
        $this->app->di = $instanceHandler;

        /**
         * setup fixtures to load
         */
        $this->setupFixtures(
            array(
                'members-fixture'
            )
        );
        $pdo_conn = $this->loadFixtures();

        $this->connection_resolver = new \Illuminate\Database\ConnectionResolver();
        $this->connection = new \Illuminate\Database\Connection($pdo_conn);
    }

    /**
     * For a registered user, the login controller create a session cookie
     */
    public function testLoginControllerCreateSessionCookieForRegisteredUser()
    {
        \Slim\Environment::mock(array(
            'REQUEST_METHOD' => 'POST',
            'slim.input' => 'username=tbei&password=',
            'CONTENT_TYPE' => 'application/x-www-form-urlencoded',
            'CONTENT_LENGTH' => 32
        ));

        $redisClient = new \Predis\Client([
            'scheme' => 'tcp',
            'host'   => '127.0.0.1',
            'port'   => 6379,
        ]);

        // Session Management
        $this->app->container->singleton(
            'storage',
            function () use ($redisClient) {
                return new RedisStorage($redisClient);
            }
        );

        $member_model = new Member();
        $model_connection_name = $member_model->getConnectionName();

        $this->connection_resolver->addConnection($model_connection_name, $this->connection);
        $member_model->setConnectionResolver($this->connection_resolver);
        $this->app->Member = $member_model;

        $lg = new LoginController($this->app);
        $lg->createAction();

        $this->assertNotEmpty($this->app->response->cookies->get(Authentication::AUTH_COOKIE_NAME));
    }
}
