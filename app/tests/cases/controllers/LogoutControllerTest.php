<?php
/**
 * testing class for Logout Controller
 *
 *
 * @subpackage Config
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\tests\cases\controllers;

use app\controllers\LogoutController;

class LogoutControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $app = null;
    protected $connection_resolver = null;
    protected $connection = null;

    /**
     * Runs one before each test
     * Set up the necessary SLIM instance
     * and fixtures XML to load
     */
    public function setUp()
    {
        $this->app = new \SlimController\Slim(
            array(
                'debug'         => false,
                'log.enabled'   => false,
                'cookies.secret_key' => 'wk4',
                'cookies.lifetime' => '1 hour',
                'controller.class_prefix'    => '\\app\\controllers',
                'models.class_prefix'        => '\\app\\models',
            )
        );
    }

    /**
     * Test the removal of a cookie
     */
    public function testLogoutControllerCookieExistAndWillBeRemoved()
    {
        \Slim\Environment::mock(array(
            'REQUEST_METHOD' => 'POST',
            'slim.input' => 'username=tbei&password=',
            'CONTENT_TYPE' => 'application/x-www-form-urlencoded',
            'CONTENT_LENGTH' => 32,
            'HTTP_COOKIE' => 'ghid=29e458d180c3f8c8f1cb7dc5b5ca0c7c'
        ));
        //Set-Cookie: ghid=29e458d180c3f8c8f1cb7dc5b5ca0c7c; path=/; expires=Wed, 17-Sep-2014 08:03:15 UTC

        $lgOut = new LogoutController($this->app);
        $lgOut->indexAction();

        $this->assertEquals(
            array(
                'status' => "OK",
                'data'   => "User Logged out."
                ),
            $this->app->output
        );
    }

    /**
     * Test the removal of a Non existent cookie
     */
    public function testLogoutControllerNotCookieFound()
    {
        \Slim\Environment::mock(array(
            'REQUEST_METHOD' => 'POST',
            'slim.input' => 'username=tbei&password=',
            'CONTENT_TYPE' => 'application/x-www-form-urlencoded',
            'CONTENT_LENGTH' => 32,
            'HTTP_COOKIE' => 'ghidNew=29e458d180c3f8c8f1cb7dc5b5ca0c7c'
        ));
        //Set-Cookie: ghid=29e458d180c3f8c8f1cb7dc5b5ca0c7c; path=/; expires=Wed, 17-Sep-2014 08:03:15 UTC

        $lgOut = new LogoutController($this->app);
        $lgOut->indexAction();

        $this->assertEquals(
            array(
                'status' => "OK",
                'data'   => "No Login session found."
            ),
            $this->app->output
        );
    }
}
