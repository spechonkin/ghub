<?php
/**
 * testing class for User Controller
 *
 * Extending FixturesConnector Class
 *
 *
 * @subpackage Config
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\tests\cases\controllers;

use app\models\Member;
use app\tests\conf\FixtureTestCase;

class UserControllerTest extends FixtureTestCase
{
    protected $app = null;
    protected $connection_resolver = null;
    protected $connection = null;

    /**
     * Runs one before each test
     * Set up the necessary SLIM instance
     * and fixtures XML to load
     */
    public function setUp()
    {
        $this->app = new \SlimController\Slim(
            array(
                'debug'         => false,
                'log.enabled'   => false,
                'cookies.secret_key' => 'wk4',
                'cookies.lifetime' => '1 hour',
                'controller.class_prefix'    => '\\app\\controllers',
                'models.class_prefix'        => '\\app\\models',
            )
        );

        /**
         * setup fixtures to load
         */
        $this->setupFixtures(
            array(
                'members-fixture'
            )
        );
        $pdo_conn = $this->loadFixtures();

        $this->connection_resolver = new \Illuminate\Database\ConnectionResolver();
        $this->connection = new \Illuminate\Database\Connection($pdo_conn);

    }

    /**
     * Function to test the JSON response from calling the index function of the controller
     */
    public function testValidJsonResponseOfControllerUserIndex()
    {
        \Slim\Environment::mock();
        $instanceHandler = new \config\Dependencies($this->app);
        $this->app->di = $instanceHandler;

        /**
         * We need the model connection name to setup the new sqlite memory connection to this same connection.
         * If we change the name of the connection on the model, some of the Eloquent functions get disable
         */
        $member_model = new Member();
        $model_connection_name = $member_model->getConnectionName();

        $this->connection_resolver->addConnection($model_connection_name, $this->connection);

        $member_model->setConnectionResolver($this->connection_resolver);
        $this->app->Member = $member_model;

        $stub = $this->getMock('app\controllers\UserController', null, array(&$this->app));

        $stub->indexAction();
        $output = json_encode($this->app->output);
        $this->assertJson($output);
    }

    /**
     * Function to test the User Show method
     */
    public function testShowAction()
    {
        \Slim\Environment::mock();

        $stub = $this->getMock('app\controllers\UserController', null, array(&$this->app));

        $member = $this->getMock('app\models\Member', null);

        $di_stub = $this->getMock('config\Dependencies', array('get'), array($this->app));

        // Configure the stub.
        $di_stub->expects($this->any())
            ->method('get')
            ->will($this->returnValue($member));

        $this->app->di = $di_stub;

        $stub->showAction('test_phpunit');

        $output = json_encode($this->app->output);
        $this->assertJson($output);
    }

    /**
     *
     */
    public function testCreateAction()
    {
        \Slim\Environment::mock();

        $stub = $this->getMock('app\controllers\UserController', null, array(&$this->app));

        $member = $this->getMockBuilder('app\models\Member')
            ->setMethods(array('save'))
            ->getMock();

        $di_stub = $this->getMock('config\Dependencies', array('get'), array($this->app));

        // Configure the stub.
        $di_stub->expects($this->any())
            ->method('get')
            ->will($this->returnValue($member));

        $this->app->di = $di_stub;

        $stub->createAction('test_phpunit');

        $output = json_encode($this->app->output);
        $this->assertJson($output);
    }
}
