<?php
/**
 * Testing class for the Authentication class
 *
 *
 * @subpackage Controller
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\tests\cases\lib;

use app\models\Member;
use app\lib\Authentication;
use app\lib\storage\RedisStorage;
use app\tests\conf\FixtureTestCase;

class AuthenticationTest extends FixtureTestCase
{
    protected $app = null;
    protected $connection_resolver = null;
    protected $connection = null;

    /**
     * Runs one before each test
     * Set up the necessary Slim instance
     */
    public function setUp()
    {
        $this->app = new \SlimController\Slim(
            array(
                'debug'         => false,
                'log.enabled'   => false,
                'cookies.secret_key' => 'wk4',
                'cookies.lifetime' => '1 hour',
                'controller.class_prefix'    => '\\app\\controllers',
                'models.class_prefix'        => '\\app\\models',
            )
        );

        /**
         * setup fixtures to load
         */
        $this->setupFixtures(
            array(
                'members-fixture'
            )
        );
        $pdo_conn = $this->loadFixtures();

        $this->connection_resolver = new \Illuminate\Database\ConnectionResolver();
        $this->connection = new \Illuminate\Database\Connection($pdo_conn);
    }

    /**
     * Function to test the authentication with a non existing cookie
     */
    public function testUserAuthenticatedIsFalse()
    {
        // Use the auto mpck environment
        \Slim\Environment::mock();

        $authenticated = new Authentication();
        $res = $authenticated->isAuthenticated($this->app);

        $this->assertFalse($res);
    }

    /**
     * Function to test the authentication with an existing cookie
     */
    public function testExistingCookieForUserAuthenticatedIsTrue()
    {
        /**
         * Dependency injector class.
         *
         */
        $instanceHandler = new \config\Dependencies($this->app);
        $this->app->di =$instanceHandler;
//        $instanceHandler->instance_models();

        // Use the auto mpck environment
        \Slim\Environment::mock();

        $redisClient = new \Predis\Client([
            'scheme' => 'tcp',
            'host'   => '127.0.0.1',
            'port'   => 6379,
        ]);

        // Session Management
        $this->app->container->singleton('storage', function () use ($redisClient) {
            return new RedisStorage($redisClient);
        });

        // Create the TEST cookie
        $hash_value = md5(\app\middleware\Authentication::AUTH_COOKIE_NAME);
        $hash = $this->app->storage->getHash($hash_value);
        $hash["user_id"] = '666';
        $hash["firstname"] = 'test_phpUnit';

        // Set the cookie, we use this way to directly insert the cookie in the request,
        // as if we use the getCookie() function wil return false, reason si because the cookie
        // will not be accessible with this method until the subsequent request
        $this->app->request->cookies->set(\app\middleware\Authentication::AUTH_COOKIE_NAME, $hash_value);


        $member_model = new Member();
        $model_connection_name = $member_model->getConnectionName();

        $this->connection_resolver->addConnection($model_connection_name, $this->connection);

        $member_model->setConnectionResolver($this->connection_resolver);
        $this->app->Member = $member_model;


        $authenticated = new Authentication($this->app);
        $res = $authenticated->isAuthenticated($this->app);

        $this->assertTrue($res);
    }

    /**
     * Function to test the authentication with an existing cookie
     */
    public function testEmptyUseridForUserAuthenticatedReturnFalse()
    {
        // Use the auto mpck environment
        \Slim\Environment::mock();

        $redisClient = new \Predis\Client([
            'scheme' => 'tcp',
            'host'   => '127.0.0.1',
            'port'   => 6379,
        ]);

        // Session Management
        $this->app->container->singleton('storage', function () use ($redisClient) {
            return new RedisStorage($redisClient);
        });

        // Create the TEST cookie
        $hash_value = md5(\app\middleware\Authentication::AUTH_COOKIE_NAME);
        $hash = $this->app->storage->getHash($hash_value);
        $hash["user_id"] = '';
        $hash["firstname"] = 'test_username';

        // Set the cookie, we use this way to directly insert the cookie in the request,
        // as if we use the getCookie() function wil return false, reason si because the cookie
        // will not be accessible with this method until the subsequent request
        $this->app->request->cookies->set(\app\middleware\Authentication::AUTH_COOKIE_NAME, $hash_value);


        $member_model = new Member();
        $model_connection_name = $member_model->getConnectionName();

        $this->connection_resolver->addConnection($model_connection_name, $this->connection);

        $member_model->setConnectionResolver($this->connection_resolver);
        $this->app->Member = $member_model;


        $authenticated = new Authentication($this->app);
        $res = $authenticated->isAuthenticated($this->app);

        $this->assertFalse($res);
    }
}
