<?php
/**
 * Testing class for lib/Authorization
 *
 *
 * @subpackage Controller
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\tests\cases\lib;

use app\lib\Authorization;

class AuthorizationTest extends \PHPUnit_Framework_TestCase
{
    protected $app = null;

    /**
     * Runs one before each test
     * Set up the necessary autoload, connection and classes
     */
    public function setUp()
    {
        $this->app = new \SlimController\Slim(
            array(
                'debug'         => false,
                'log.enabled'   => false,
            )
        );

        //Remove environment mode if set
        unset($_ENV['SLIM_MODE']);

        //Reset session
        $_SESSION = array();

        //Prepare default environment variables
        \Slim\Environment::mock(array(
//            'SCRIPT_NAME' => '/foo', //<-- Physical
            'PATH_INFO' => '/bar', //<-- Virtual
        ));
    }


    /**
     * Function to test the authorization, When Controller not found for the Route must return false
     */
    public function testIsNotAuthorizedFunction()
    {
        $router = new \Slim\Router();
        $route = new \Slim\Route(
            '/foo',
            function () {
            }
        );

        $propertyMatchedRoutes = new \ReflectionProperty($router, 'matchedRoutes');
        $propertyMatchedRoutes->setAccessible(true);
        $propertyMatchedRoutes->setValue($router, array($route));

        $propertyCurrentRoute = new \ReflectionProperty($route, 'name');
        $propertyCurrentRoute->setAccessible(true);
        $propertyCurrentRoute->setValue($route, 'TestphpunitController:index');

        $this->app->router = $router;

        $authorized = new Authorization($this->app);
        $res = $authorized->isAuthorized();
        $this->assertFalse($res);
    }

    /**
     * Function to test the authorization, When Controller found must return true
     */
    public function testIsAuthorizedIndexFunction()
    {
        $router = new \Slim\Router();
        $route = new \Slim\Route(
            '/foo',
            function () {
            }
        );

        $propertyMatchedRoutes = new \ReflectionProperty($router, 'matchedRoutes');
        $propertyMatchedRoutes->setAccessible(true);
        $propertyMatchedRoutes->setValue($router, array($route));

        $propertyCurrentRoute = new \ReflectionProperty($route, 'name');
        $propertyCurrentRoute->setAccessible(true);
        $propertyCurrentRoute->setValue($route, 'UserController:index');

        $this->app->router = $router;

        $authorized = new Authorization($this->app);
        $res = $authorized->isAuthorized();
        $this->assertTrue($res);
    }


    /**
     * Function to test the authorization, When Controller found must return true
     */
    public function testIsAuthorizedShowFunction()
    {
        $router = new \Slim\Router();
        $route = new \Slim\Route(
            '/foo',
            function () {
            }
        );

        $propertyMatchedRoutes = new \ReflectionProperty($router, 'matchedRoutes');
        $propertyMatchedRoutes->setAccessible(true);
        $propertyMatchedRoutes->setValue($router, array($route));

        $propertyCurrentRoute = new \ReflectionProperty($route, 'name');
        $propertyCurrentRoute->setAccessible(true);
        $propertyCurrentRoute->setValue($route, 'UserController:show');

        $this->app->router = $router;

        $authorized = new Authorization($this->app);
        $res = $authorized->isAuthorized();
        $this->assertTrue($res);
    }
}
