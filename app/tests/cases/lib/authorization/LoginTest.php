<?php
/**
 * testing authorization Login class
 *
 *
 * @subpackage lib/authorization
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\tests\cases\lib\authorization;

use app\lib\authorization\Login;

class LoginTest extends \PHPUnit_Framework_TestCase
{
    protected static $login = null;
    /**
     * Runs one before the test
     */
    public static function setUpBeforeClass()
    {
        $app = new \SlimController\Slim(
            array(
                'debug'         => false,
                'log.enabled'   => false,
            )
        );
        self::$login = new Login($app);
    }

    /**
     * Assert on return default value false
     */
    public function testIndexReturnDefaultFalse()
    {
        $this->assertFalse(self::$login->index());
    }

    /**
     * Assert on return default value false
     */
    public function testShowReturnDefaultFalse()
    {
        $this->assertFalse(self::$login->show());
    }

    /**
     * Test that create is active for login and will return true
     */
    public function testValidCreate()
    {
        $this->assertTrue(self::$login->create());
    }

    /**
     * Assert on return default value false
     */
    public function testUpdateReturnDefaultFalse()
    {
        $this->assertFalse(self::$login->Update());
    }

    /**
     * Assert on return default value false
     */
    public function testDeleteReturnDefaultFalse()
    {
        $this->assertFalse(self::$login->delete());
    }
}
