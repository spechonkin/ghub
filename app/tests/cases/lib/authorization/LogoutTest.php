<?php
/**
 * testing authorization Logout class
 *
 *
 * @subpackage lib/authorization
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\tests\cases\lib\authorization;

use app\lib\authorization\Logout;

class LogoutTest extends \PHPUnit_Framework_TestCase
{
    protected static $logout = null;
    /**
     * Runs one before the test
     */
    public static function setUpBeforeClass()
    {
        $app = new \SlimController\Slim(
            array(
                'debug'         => false,
                'log.enabled'   => false,
            )
        );
        self::$logout = new Logout($app);
    }

    /**
     * Test that index is active for logout and will return true
     */
    public function testValidIndex()
    {
        $this->assertTrue(self::$logout->index());
    }

    /**
     * Assert on return default value false
     */
    public function testShowReturnDefaultFalse()
    {
        $this->assertFalse(self::$logout->show());
    }

    /**
     * Assert on return default value false
     */
    public function testCreateReturnDefaultFalse()
    {
        $this->assertFalse(self::$logout->create());
    }

    /**
     * Assert on return default value false
     */
    public function testUpdateReturnDefaultFalse()
    {
        $this->assertFalse(self::$logout->Update());
    }

    /**
     * Assert on return default value false
     */
    public function testDeleteReturnDefaultFalse()
    {
        $this->assertFalse(self::$logout->delete());
    }
}
