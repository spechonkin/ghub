<?php
/**
 * testing Default values in ResourceAuthorization
 *
 *
 * @subpackage lib/authorization
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\tests\cases\lib\authorization;

use app\lib\authorization\ResourceAuthorization;

class ResourceAuthorizationTest extends \PHPUnit_Framework_TestCase
{
    protected $res_auth = null;

    /**
     * Runs one before each test
     * Set up the necessary ResourceAuthorization instance
     */
    public function setUp()
    {
        $this->res_auth = new ResourceAuthorization(new \SlimController\Slim());
    }

    /**
     * Assert on return default value false
     */
    public function testIndexReturnDefaultFalse()
    {
        $this->assertFalse($this->res_auth->index());
    }

    /**
     * Assert on return default value false
     */
    public function testShowReturnDefaultFalse()
    {
        $this->assertFalse($this->res_auth->show());
    }

    /**
     * Assert on return default value false
     */
    public function testCreateReturnDefaultFalse()
    {
        $this->assertFalse($this->res_auth->create());
    }

    /**
     * Assert on return default value false
     */
    public function testUpdateReturnDefaultFalse()
    {
        $this->assertFalse($this->res_auth->Update());
    }

    /**
     * Assert on return default value false
     */
    public function testDeleteReturnDefaultFalse()
    {
        $this->assertFalse($this->res_auth->delete());
    }
}
