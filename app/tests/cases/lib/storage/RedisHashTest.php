<?php
/**
 * Testing class for RedisHash
 *
 *
 * @subpackage lib/storage
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\tests\cases\lib\storage;

use app\lib\storage\RedisHash;
use app\lib\storage\RedisStorage;

class RedisHashTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the exception of the hash name for the Radis client
     *
     * @expectedException        \InvalidArgumentException
     * @expectedExceptionCode    0
     * @expectedExceptionMessage The hash has to be named!
     */
    public function testExceptionInConstructor()
    {
        $redisClient = new \Predis\Client([
            'scheme' => 'tcp',
            'host'   => '127.0.0.1',
            'port'   => 6379,
        ]);

        $rs = new RedisHash($redisClient, '');
    }


    /**
     * Test the return of an Array Iterator form the Radishash
     */
    public function testRedisHashGetiteratorReturnArrayiterator()
    {
        $redisClient = new \Predis\Client([
            'scheme' => 'tcp',
            'host'   => '127.0.0.1',
            'port'   => 6379,
        ]);

        $red_storage = new RedisStorage($redisClient);
        $hash_value = md5('test_phpunit_redis');

        $red_hash = $red_storage->getHash($hash_value);

        $this->assertInstanceOf('\ArrayIterator', $red_hash->getIterator());
    }


    /**
     * test the RadisHash set and get
     */
    public function testRedisHashSetiteratorReturnArrayiterator()
    {
        $redisClient = new \Predis\Client([
            'scheme' => 'tcp',
            'host'   => '127.0.0.1',
            'port'   => 6379,
        ]);

        $red_storage = new RedisStorage($redisClient);
        $hash_value = md5('test_phpunit_redis');

        $red_hash = $red_storage->getHash($hash_value);

        $red_hash->offsetSet('test_offset', 0);

        $this->assertEquals(0, $red_hash->offsetGet('test_offset'));
    }


    /**
     * Test the correct unset of the Radishash
     */
    public function testRedisHashUnsetiteratorReturnArrayiterator()
    {
        $redisClient = new \Predis\Client([
            'scheme' => 'tcp',
            'host'   => '127.0.0.1',
            'port'   => 6379,
        ]);

        $red_storage = new RedisStorage($redisClient);
        $hash_value = md5('test_phpunit_redis');

        $red_hash = $red_storage->getHash($hash_value);

        $red_hash->offsetSet('test_offset', 0);
        $red_hash->offsetUnset('test_offset', 0);

        $this->assertNull($red_hash->offsetGet('test_offset'));
    }


    /**
     * Test the correct delete of the hash by expiring the RadisHash
     */
    public function testRedisHashExpire()
    {
        $redisClient = new \Predis\Client([
            'scheme' => 'tcp',
            'host'   => '127.0.0.1',
            'port'   => 6379,
        ]);

        $red_storage = new RedisStorage($redisClient);
        $hash_value = md5('test_phpunit_redis');

        $red_hash = $red_storage->getHash($hash_value, 1);

        $red_hash->offsetSet('test_offset', 666);
        // sleep for 1 seconds to let the hash expire
        sleep(2);

        $this->assertNull($red_hash->offsetGet('test_offset'));
    }
}
