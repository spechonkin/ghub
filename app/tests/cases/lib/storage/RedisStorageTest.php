<?php
/**
 * Testing class for RedisStorage
 *
 *
 * @subpackage lib/storage
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\tests\cases\lib\storage;

use app\lib\storage\RedisStorage;

class RedisStorageTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the correct working of the setter function on Radis client
     */
    public function testValidClientSetter()
    {
        $redisClient = new \Predis\Client([
            'scheme' => 'tcp',
            'host'   => '127.0.0.1',
            'port'   => 6379,
        ]);

        $rs = new RedisStorage($redisClient);

        $rs->set('testPHPUNIT', 0);

        $this->assertEquals(0, $rs->get('testPHPUNIT'));
    }

    /**
     * The the correct get for a particular set of RadisClient
     */
    public function testInvalidClientSetter()
    {
        $redisClient = new \Predis\Client([
            'scheme' => 'tcp',
            'host'   => '127.0.0.1',
            'port'   => 6379,
        ]);

        $rs = new RedisStorage($redisClient);

        $rs->set('testPHPUNIT', 1);

        $this->assertNotEquals(0, $rs->get('testPHPUNIT'));
    }


    /**
     * Test the correct delete of a set for the RadisClient
     */
    public function testDeleteClientVariable()
    {
        $redisClient = new \Predis\Client([
            'scheme' => 'tcp',
            'host'   => '127.0.0.1',
            'port'   => 6379,
        ]);

        $rs = new RedisStorage($redisClient);

        $rs->set('testPHPUNIT', 1);

        $rs->delete('testPHPUNIT');

        $this->assertNull($rs->get('testPHPUNIT'));
    }

    /**
     * Test the correct working of the set, by deleteing a single set and validating the existance of the other.
     */
    public function testDeleteSingleClientVariable()
    {
        $redisClient = new \Predis\Client([
            'scheme' => 'tcp',
            'host'   => '127.0.0.1',
            'port'   => 6379,
        ]);

        $rs = new RedisStorage($redisClient);

        $rs->set('testPHPUNIT_0', 0);
        $rs->set('testPHPUNIT_1', 1);

        $rs->delete('testPHPUNIT_0');

        $value_0 = $rs->get('testPHPUNIT_0');
        $value_1 = $rs->get('testPHPUNIT_1');

        $this->assertNull($value_0);
        $this->assertEquals(1, $value_1);
    }
}
