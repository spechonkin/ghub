<?php
/**
 * testing Middleware Authentication
 *
 *
 * @subpackage Cconfig
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\tests\cases\middleware;

class AuthenticationTest extends \PHPUnit_Framework_TestCase
{
    protected $app = null;

    /**
     * Runs one before each test
     * Set up the necessary autoload, connection and classes
     */
    public function setUp()
    {
        $this->app = new \Slim\Slim(
            array(
                'debug'         => false,
                'log.enabled'   => false,
            )
        );

        //Remove environment mode if set
        unset($_ENV['SLIM_MODE']);

        //Reset session
        $_SESSION = array();

        //Prepare default environment variables
        \Slim\Environment::mock(array(
            'PATH_INFO' => '/bar', //<-- Virtual
        ));
    }

    /**
     * Test to probe the correct set of the authentication hook
     */
    public function testAuthorizationHookCanBeSet()
    {
        $mwAuth = new \app\middleware\Authentication();
        $mwAuth->setApplication($this->app);
        $mwAuth->setNextMiddleware($this->app);
        $mwAuth->call();

        $hooks = $this->app->getHooks('slim.before.dispatch');
        $hook_authorization = array_values(array_filter($hooks));

        $this->assertGreaterThan(0, count($hook_authorization));
    }

    /**
     * Test that validate the exception of the unauthorized access
     * on the Authentication hook
     *
     * @expectedException         \app\lib\exceptions\HttpUnauthorizedException
     * @expectedExceptionCode     401
     */
    public function testApplyHook()
    {
        $mwAuth = new \app\middleware\Authentication();
        $mwAuth->setApplication($this->app);
        $mwAuth->setNextMiddleware($this->app);
        $mwAuth->call();

        $this->app->applyHook('slim.before.dispatch');
    }
}
