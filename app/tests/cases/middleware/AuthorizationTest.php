<?php
/**
 * testing Middleware Authorization
 *
 *
 * @subpackage Cconfig
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\tests\cases\middleware;

class AuthorizationTest extends \PHPUnit_Framework_TestCase
{
    protected $app = null;

    /**
     * Runs one before each test
     * Set up the necessary autoload, connection and classes
     */
    public function setUp()
    {
        $this->app = new \SlimController\Slim(
            array(
                'debug'         => false,
                'log.enabled'   => false,
            )
        );

        //Remove environment mode if set
        unset($_ENV['SLIM_MODE']);

        //Reset session
        $_SESSION = array();

        //Prepare default environment variables
        \Slim\Environment::mock(array(
            'PATH_INFO' => '/bar', //<-- Virtual
        ));
    }


    /**
     * Test correct load of authorization class
     *
     */
    public function testAuthorizationMiddlewareCallExist()
    {
        $mwAuth = new \app\middleware\Authorization();
        $mwAuth->setApplication($this->app);
        $mwAuth->setNextMiddleware($this->app);
        $mwAuth->call();

        $hooks = $this->app->getHooks('slim.before.dispatch');
        $hook_authorization = array_values(array_filter($hooks));

        $this->assertGreaterThan(0, count($hook_authorization));
    }

    /**
     * Test that validate the exception of the unauthorized access
     * on the Authorization hook
     *
     * @expectedException         \app\lib\exceptions\HttpForbiddenException
     * @expectedExceptionCode     403
     */
    public function testApplyHook()
    {
        $router = new \Slim\Router();

        // Create a stub for the SomeClass class.
        $route = $this->getMockBuilder('\Slim\Route')
            ->setMethods(array('getName'))
            ->setConstructorArgs(
                array(
                    '/bar',
                    function () {
                    }
                )
            )
            ->getMock();

        // Configure the stub.
        $route->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('UserController:create'));

        $propertyMatchedRoutes = new \ReflectionProperty($router, 'matchedRoutes');
        $propertyMatchedRoutes->setAccessible(true);
        $propertyMatchedRoutes->setValue($router, array($route));

        $this->app->router = $router;

        $mwAuth = new \app\middleware\Authorization();
        $mwAuth->setApplication($this->app);
        $mwAuth->setNextMiddleware($this->app);
        $mwAuth->call();

        $this->app->applyHook('slim.before.dispatch');
    }
}
