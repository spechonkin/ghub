<?php
/**
 * testing model Member class
 *
 *
 * @subpackage lib/authorization
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\tests\cases\models;

use app\models\Member;

class MemberTest extends \PHPUnit_Framework_TestCase
{
    /**
     * test valid creation of a member instance
     */
    public function testValidMemberClassInstance()
    {
        $member = new Member();
        $this->assertInstanceOf('app\models\Member', $member);
    }
}
