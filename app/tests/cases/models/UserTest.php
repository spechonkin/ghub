<?php
/**
 * testing model User class
 *
 *
 * @subpackage lib/authorization
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\tests\cases\models;

use app\models\User;

class UserTest extends \PHPUnit_Framework_TestCase
{
    /**
     * test valid creation of user instance
     */
    public function testValidMemberClassInstance()
    {
        $user = new User();
        $this->assertInstanceOf('app\models\User', $user);
    }
}
