<?php
/**
 * Partial test class for services -> Bcrypt class
 *
 *
 * @subpackage Services
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace {

    // This allow us to configure the behavior of the "global mock"
    $mockSocketCreate = 1;
}

namespace app\services {

    /**
     * Override php define function.
     * We use the same count parameter to define the kind of result expected depending on the test.
     * @param $count
     * @return mixed|null|string
     */
    function openssl_random_pseudo_bytes($count)
    {
        global $mockSocketCreate;

        if ($mockSocketCreate == 1) {
            return null;
        } elseif ($mockSocketCreate == 2) {
            return '';
        } else {
            return call_user_func_array('\openssl_random_pseudo_bytes', func_get_args());
        }
    }


    class BcryptGetRandomTest extends \PHPUnit_Framework_TestCase
    {
        /**
         * Test the return of a byte string when function openssl_random_pseudo_bytes is not defined
         */
        public function testGetRandomBytes()
        {
            global $mockSocketCreate;
            $mockSocketCreate = 1;

            $fixture = new Bcrypt(12, 'salt_salt');

            $class = new \ReflectionClass($fixture);
            $method = $class->getMethod('getRandomBytes');
            $method->setAccessible(true);
            $output = $method->invoke($fixture, 10);

            $this->assertNotEmpty($output);
        }

        /**
         * Test the return of a byte string when function
         * openssl_random_pseudo_bytes is defined but returned an empty string
         */
        public function testFunctionOpensslFunctionDoesNotExist()
        {
            global $mockSocketCreate;
            $mockSocketCreate = 2;

            $fixture = new Bcrypt(12, 'salt_salt');

            $class = new \ReflectionClass($fixture);
            $method = $class->getMethod('getRandomBytes');
            $method->setAccessible(true);
            $output = $method->invoke($fixture, 12);

            $this->assertNotEmpty($output);
        }
    }
}
