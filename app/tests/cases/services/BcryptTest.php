<?php
/**
 * Partial test class for services -> Bcrypt class
 *
 *
 * @subpackage Services
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */

namespace app\tests\cases\services;

use app\services\Bcrypt;

class BcryptTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the return of a False hash
     */
    public function testHashFunctionReturnIncorrectHash()
    {
        $bc = new Bcrypt(12, 'salt_salt');
        $this->assertFalse($bc->hash('test_string'));
    }

    /**
     * Test the return of a correct hash
     */
    public function testHashFunctionReturnCorrectHash()
    {
        $bc = new Bcrypt(12, null);
        $this->assertStringStartsWith('$', $bc->hash('test_string'));
    }
}
