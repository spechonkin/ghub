<?php
/**
 * Fixture Connection class.
 *
 * This class is in charge of creating the sqlite::memory connection for the testing
 * and setting up the fixtures based on the xml files under path:   test->conf->fixtures
 *
 * http://someguyjeremy.com/blog/database-testing-with-phpunit
 * https://gist.github.com/jeremyharris/4484784
 *
 *
 * @subpackage Config
 */

namespace app\tests\conf;

class FixtureTestCase extends \PHPUnit_Extensions_Database_TestCase
{

    /**
     * Array to define the fixtures to load
     * @var array
     */
    protected $fixtures = array();

    private $conn = null;

    public function setupFixtures($fixtures)
    {
        $this->fixtures = $fixtures;
    }

    public function loadFixtures()
    {
        $conn = $this->getConnection();
        $pdo = $conn->getConnection();

        // set up tables
        $fixtureDataSet = $this->getDataSet($this->fixtures);
        foreach ($fixtureDataSet->getTableNames() as $table) {
            // drop table
            $pdo->exec("DROP TABLE IF EXISTS `$table`;");
            // recreate tables
            $meta = $fixtureDataSet->getTableMetaData($table);
            $create = "CREATE TABLE IF NOT EXISTS `$table` ";
            $cols = array();
            foreach ($meta->getColumns() as $col) {
                $cols[] = "`$col` VARCHAR(200)";
            }
            $create .= '('.implode(',', $cols).');';
            $pdo->exec($create);
        }

        parent::setUp();
        return $pdo;
    }

    public function tearDown()
    {
        $allTables =
            $this->getDataSet($this->fixtures)->getTableNames();
        foreach ($allTables as $table) {
            // drop table
            $conn = $this->getConnection();
            $pdo = $conn->getConnection();
            $pdo->exec("DROP TABLE IF EXISTS `$table`;");
        }

        $pdo = null;
        parent::tearDown();
    }

    public function getConnection()
    {
        require dirname(dirname(dirname(__DIR__))) . '/config/settings.php';

        if ($this->conn === null) {
            try {
                $setting = $connections[0];

                $driver   = $setting['driver'];
                $database = $setting['database'];
                $options  = $setting['options'];
                $name     = $setting['name'];

                $pdo = new \PDO(
                    $driver . ':' . $database,
                    null,
                    null,
                    $options
                );
                $this->conn = $this->createDefaultDBConnection($pdo, $name);

            } catch (\PDOException $e) {
                echo $e->getMessage();
            }
        }
        return $this->conn;
    }

    public function getDataSet($fixtures = array())
    {
        if (empty($fixtures)) {
            $fixtures = $this->fixtures;
        }
        $compositeDs = new
        \PHPUnit_Extensions_Database_DataSet_CompositeDataSet(array());
        $fixturePath = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'fixtures';

        foreach ($fixtures as $fixture) {
            $path =  $fixturePath . DIRECTORY_SEPARATOR . "$fixture.xml";
            $ds = $this->createMySQLXMLDataSet($path);
            $compositeDs->addDataSet($ds);
        }
        return $compositeDs;
    }

    public function loadDataSet($dataSet)
    {
        // set the new dataset
        $this->getDatabaseTester()->setDataSet($dataSet);
        // call setUp whateverhich adds the rows
        $this->getDatabaseTester()->onSetUp();
    }
}
