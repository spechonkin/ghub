<?php
/**
 * Connection class.
 *
 * The class receive the connections string defined in the settings file,
 * then it will create a connection for each one of the connections defined.
 * This connections are set in the Eloquent resolver and the placed inside the
 * Eloquent Model.
 *
 * @subpackage Config
 */

namespace config;

class Connection extends \Slim\Middleware
{
    public function call()
    {
        $connFactory = new \Illuminate\Database\Connectors\ConnectionFactory(new \Illuminate\Container\Container);
        $resolver = new \Illuminate\Database\ConnectionResolver();

        $default_db = $this->app->connections[0]['name'];

        $conn_maker = function($connection) use($connFactory, $resolver) {
            $pdo_conn = $connFactory->make($connection);
            $resolver->addConnection($connection['database'], $pdo_conn);
        };
        array_map($conn_maker, $this->app->connections);

        $resolver->setDefaultConnection($default_db);

        \Illuminate\Database\Eloquent\Model::setConnectionResolver($resolver);
        $this->next->call();
    }
}
