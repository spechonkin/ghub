<?php
/**
 * Dependency injector class.
 * Create instances of model class and set them in SLIM
 *
 *
 * @subpackage Controller
 * @author Rodrigo De la garza <rodrigo.delagarza@glispamedia.com>
 */
namespace config;

class Dependencies
{
    protected $app, $container, $settings_prefix;

    public function __construct(\SlimController\Slim $app)
    {
        $this->app = $app;
        $this->settings_prefix = $app->settings;
    }

    /**
     * Check for the instance of the class in SLIM DI container,
     * if not found create a new instance of a class, and added it to SLIM DI container
     * Otherwise return the found class.
     *
     * @param $original_class_name  : the original name of the class
     * @param string $optional_class_name   : alias that can be assigned to the setter (optional)
     * @param string $class_namespace   : the type of class to instanciate (default model)
     * @return mixed
     */
    public function get($original_class_name, $optional_class_name = '', $class_namespace = 'models')
    {
        $class_path = $this->settings_prefix[$class_namespace . '.class_prefix'];
        $class = $class_path .'\\'. $original_class_name;

        $class_obj = new \stdClass();
        if (class_exists($class)) {
            $class_obj = new $class;
        }

        $di_class_name = $original_class_name;
        if(!empty($optional_class_name)) {
            $di_class_name = $optional_class_name;
        }

        if(!$this->has($di_class_name)) {
            $this->app->{$di_class_name} = function () use ($class_obj) {
            return $class_obj;
            };
        }
        return $this->app->{$di_class_name};
    }

    /**
     * Create a new instance of the class and add it to the dependency inyector.
     *
     * @param $original_class_name  : the original name of the class
     * @param string $optional_class_name   : alias that can be assigned to the setter (optional)
     * @param array $parameters
     * @param string $class_namespace   : the type of class to instanciate (default model)
     * @return mixed
     */
    public function make($original_class_name, $optional_class_name = '', $parameters = array(), $class_namespace = 'models')
    {
        $class_path = $this->settings_prefix[$class_namespace . '.class_prefix'];
        $class = $class_path .'\\'. $original_class_name;

        $class_obj = new \stdClass();
        if (class_exists($class)) {
            $class_obj = new $class;
        }

        $di_class_name = $original_class_name;
        if (!empty($optional_class_name)) {
            $di_class_name = $optional_class_name;
        }

        $this->app->{$di_class_name} = function () use ($class_obj) {
            return $class_obj;
        };

        return $this->app->{$di_class_name};
    }

    /**
     * Method to see if the class variable is assigned already
     *
     * @param $class_name
     * @return bool
     */
    public function has($class_name){
        if(empty($this->app->{$class_name})) {
            return false;
        }
        return true;
    }
}
