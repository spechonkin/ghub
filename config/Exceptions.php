<?php

namespace config;

/** 
 * Handle internal exceptions
 *
 * 
 */
class Exceptions
{
    protected $app;

    public function __construct(\Slim\Slim $app)
    {
        $this->app = $app;
    }

    public function addExceptionHandlers()
    {
        $this->app->log->debug("adding exception handlers");
        $this->app->error(
            function (\Exception $e) {
                switch ($e->getCode()) {
                    case 401:
                        $this->app->log->error("Got Exception ". $e->getCode());
                        break;
                    case 403:
                        $this->app->log->error("Got Exception ". $e->getCode());
                        break;
                    case 404:
                        $this->app->log->error("Got Exception ". $e->getCode());
                        break;
                    default:
                        $this->app->log->error("Got Exception ". $e->getCode());
                }
                $this->app->status($e->getCode());
                $this->app->output = array(
                    'status' => 'Fail',
                    'message' => $e->getMessage(),
                    'statusCode' => $e->getCode()
                );
//                \app\middleware\ContentNegotiation::negotiateContent();
            }
        );
    }
}
