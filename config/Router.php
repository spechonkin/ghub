<?php

namespace config;

class Router
{
    protected $app;
    public function __construct($app)
    {
        $this->app = $app;
    }

    public function resource($resource, $options = array())
    {
        $this->app->addRoutes(array(
            '/' . $resource . '/' => array('get' => ucfirst($resource) . 'Controller:index'),
        ));

        $this->app->addRoutes(array(
            '/' . $resource . '/:id' => array('get' => ucfirst($resource) . 'Controller:show'),
        ));

        $this->app->addRoutes(array(
            '/' . $resource . '/' => array('post' => ucfirst($resource) . 'Controller:create'),
        ));
    }
}
