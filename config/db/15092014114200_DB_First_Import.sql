
-- -----------------------------------------------------
-- Table `contact`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `contact` ;

CREATE  TABLE IF NOT EXISTS `contact` (
  `id` INT UNSIGNED NOT NULL ,
  `status` INT NULL ,
  `updated_at` DATETIME NULL ,
  `updated_by` INT NULL ,
  `created_by` INT NULL ,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `permissions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `permissions` ;

CREATE  TABLE IF NOT EXISTS `permissions` (
  `id` INT NOT NULL ,
  `user_id` INT NULL ,
  `resource_type_id` INT NULL ,
  `mask` BINARY NULL ,
  `created_at` TIMESTAMP NULL ,
  `created_by` INT NULL ,
  `updated_at` TIMESTAMP NULL ,
  `updated_by` INT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;






-- -----------------------------------------------------
-- Table `user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user` ;

CREATE  TABLE IF NOT EXISTS `user` (
  `id` INT UNSIGNED NOT NULL ,
  `uname` VARCHAR(64) NOT NULL ,
  `password` VARCHAR(255) NOT NULL ,
  `salt` VARCHAR(64) NULL ,
  `email` VARCHAR(64) NULL ,
  `created_by` SMALLINT UNSIGNED NULL ,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
  `state` INT UNSIGNED NOT NULL DEFAULT 1 ,
  `contact_id` INT UNSIGNED NOT NULL ,
  `machine` TINYINT(1) NULL DEFAULT false ,
  `permissions_id` INT NOT NULL ,
  `updated_by` INT NULL ,
  `updated_at` TIMESTAMP NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `uname_UNIQUE` (`uname` ASC) ,
  INDEX `fk_user_1_idx` (`contact_id` ASC) ,
  INDEX `fk_user_permissions1_idx` (`permissions_id` ASC) ,
  CONSTRAINT `fk_user_1`
    FOREIGN KEY (`contact_id` )
    REFERENCES `contact` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_permissions1`
    FOREIGN KEY (`permissions_id` )
    REFERENCES `permissions` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `failed_login`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `failed_login` ;

CREATE  TABLE IF NOT EXISTS `failed_login` (
  `user_id` INT UNSIGNED NOT NULL ,
  `failed_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
  `source_ip` VARBINARY(16) NOT NULL ,
  INDEX `fk_failed_user_idx` (`user_id` ASC) ,
  CONSTRAINT `fk_failed_user`
    FOREIGN KEY (`user_id` )
    REFERENCES `user` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metadata`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `metadata` ;

CREATE  TABLE IF NOT EXISTS `metadata` (
  `id` INT UNSIGNED NOT NULL ,
  `name` VARCHAR(64) NOT NULL ,
  `gui_type` VARCHAR(45) NULL ,
  `metadatacol` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contact_team_history`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `contact_team_history` ;

CREATE  TABLE IF NOT EXISTS `contact_team_history` (
  `contact_id` INT UNSIGNED NOT NULL ,
  `team` VARCHAR(45) NOT NULL ,
  `start_date` DATETIME NOT NULL ,
  `end_date` DATETIME NULL ,
  PRIMARY KEY (`contact_id`, `team`) ,
  CONSTRAINT `fk_team_member`
    FOREIGN KEY (`contact_id` )
    REFERENCES `contact` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `team`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `team` ;

CREATE  TABLE IF NOT EXISTS `team` (
  `name` VARCHAR(45) NOT NULL ,
  `image` VARCHAR(45) NULL ,
  `description` MEDIUMTEXT NULL ,
  PRIMARY KEY (`name`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `resource_metadata`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `resource_metadata` ;

CREATE  TABLE IF NOT EXISTS `resource_metadata` (
  `resource_id` INT UNSIGNED NOT NULL ,
  `metadata_id` INT UNSIGNED NOT NULL ,
  `value` VARCHAR(255) NULL ,
  `resource_type` ENUM('contanct','campaign') NULL ,
  `language` VARCHAR(45) NULL ,
  PRIMARY KEY (`resource_id`, `metadata_id`) ,
  CONSTRAINT `fk_member_info_type0`
    FOREIGN KEY (`resource_id` )
    REFERENCES `metadata` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `company`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `company` ;

CREATE  TABLE IF NOT EXISTS `company` (
  `company_id` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`company_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contact_company`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `contact_company` ;

CREATE  TABLE IF NOT EXISTS `contact_company` (
  `contact_id` INT UNSIGNED NOT NULL ,
  `company_id` INT UNSIGNED NOT NULL ,
  `contact_role` VARCHAR(45) NULL ,
  `created_by` INT NULL ,
  `updated_by` INT NULL ,
  `updated_at` TIMESTAMP NULL ,
  `created_at` TIMESTAMP NULL ,
  PRIMARY KEY (`contact_id`, `company_id`) ,
  INDEX `fk_company_idx` (`company_id` ASC) ,
  CONSTRAINT `fk_contact`
    FOREIGN KEY (`contact_id` )
    REFERENCES `contact` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_company`
    FOREIGN KEY (`company_id` )
    REFERENCES `company` (`company_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `user_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user_role` ;

CREATE  TABLE IF NOT EXISTS `user_role` (
  `role` VARCHAR(45) NOT NULL ,
  `mask` INT UNSIGNED NULL ,
  PRIMARY KEY (`role`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `user_rule`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user_rule` ;

CREATE  TABLE IF NOT EXISTS `user_rule` (
  `rule` VARCHAR(45) NOT NULL ,
  `mask` INT UNSIGNED NULL ,
  PRIMARY KEY (`rule`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contact_team`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `contact_team` ;

CREATE  TABLE IF NOT EXISTS `contact_team` (
  `contact_id` INT UNSIGNED NOT NULL ,
  `team` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`contact_id`, `team`) ,
  INDEX `fk_contact_team_2_idx` (`team` ASC) ,
  CONSTRAINT `fk_contact_team_1`
    FOREIGN KEY (`contact_id` )
    REFERENCES `contact` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contact_team_2`
    FOREIGN KEY (`team` )
    REFERENCES `team` (`name` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metadata_schema`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `metadata_schema` ;

CREATE  TABLE IF NOT EXISTS `metadata_schema` (
  `id` INT NOT NULL ,
  `resource_type` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metadata_group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `metadata_group` ;

CREATE  TABLE IF NOT EXISTS `metadata_group` (
  `metadata_id` INT UNSIGNED NOT NULL ,
  `metadata_schema_id` INT NOT NULL ,
  PRIMARY KEY (`metadata_id`, `metadata_schema_id`) ,
  INDEX `fk_metadata_group_2_idx` (`metadata_schema_id` ASC) ,
  CONSTRAINT `fk_metadata_group_1`
    FOREIGN KEY (`metadata_id` )
    REFERENCES `metadata` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_metadata_group_2`
    FOREIGN KEY (`metadata_schema_id` )
    REFERENCES `metadata_schema` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metadata_preset`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `metadata_preset` ;

CREATE  TABLE IF NOT EXISTS `metadata_preset` (
  `id` INT UNSIGNED NOT NULL ,
  `metadata_id` INT UNSIGNED NOT NULL ,
  `position` INT NULL ,
  `value` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_metadata_preset_1_idx` (`metadata_id` ASC) ,
  CONSTRAINT `fk_metadata_preset_1`
    FOREIGN KEY (`metadata_id` )
    REFERENCES `metadata` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `log`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `log` ;

CREATE  TABLE IF NOT EXISTS `log` (
  `id` INT UNSIGNED NOT NULL ,
  `user_id` INT UNSIGNED NULL ,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
  `resource_type` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

