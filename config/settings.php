<?php

$connections = array(
    array(
        'driver'    => 'mysql',
        'host'      => 'localhost',
        //'port'      => 3306,
        'database'  => 'wk3',
        'username'  => 'root',
        'password'  => 'password',
        'prefix'    => '',
        'charset'   => "utf8",
        'collation' => "utf8_general_ci",
        'profiler'  => true,
        'name'      => 'wk3'
    ),
    array(
        'driver'    => 'mysql',
        'host'      => 'localhost',
        //'port'      => 3306,
        'database'  => 'slimtest',
        'username'  => 'root',
        'password'  => 'password',
        'prefix'    => '',
        'charset'   => "utf8",
        'collation' => "utf8_general_ci",
        'profiler'  => true,
        'name'      => 'slimtest'
    )
);

// Redis Client
$redisClient = new \Predis\Client([
    'scheme' => 'tcp',
    'host'   => '127.0.0.1',
    'port'   => 6379,
]);