<?php

/**
 * SOME COMENT HEREREREEEE
 */

date_default_timezone_set('UTC');

define('APP_PATH', dirname(__DIR__));
require APP_PATH . '/vendor/autoload.php';
require APP_PATH . '/config/Router.php';
require APP_PATH . '/config/settings.php';

spl_autoload_register(function ($className) {
    $className = ltrim($className, '\\');
    $fileName = '';
    if ($lastNsPos = strripos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    // FIXME: handle this in a nice way?
    if (file_exists(APP_PATH.'/'.$fileName)) {
        require APP_PATH . '/' . $fileName;
    }
});

$app = new \SlimController\Slim(array(
    'controller.class_prefix'    => '\\app\\controllers',
    'models.class_prefix'        => '\\app\\models',
    'controller.method_suffix'   => 'Action',
    'log.enabled' => true,
    'log.level' => \Slim\Log::DEBUG,
    //'cookies.encrypt' => true,
    'cookies.secret_key' => 'wk4',
    'cookies.lifetime' => '1 hour',
    //'cookies.cipher' => MCRYPT_RIJNDAEL_256,
    //'cookies.cipher_mode' => MCRYPT_MODE_CBC
//    'debug' => false,
));
/**
 * Middleware connection setup
 */
$app->connections = $connections;
$app->add(new \config\Connection());

/**
 * Middleware, order is important!
 */
$app->add(new app\middleware\Authorization());
$app->add(new app\middleware\ContentNegotiation());

if (str_replace('/', '', $app->request()->getPathInfo()) != 'login' &&
    str_replace('/', '', $app->request()->getPathInfo()) != 'logout') {
    $app->add(new \app\middleware\Authentication($app));

    /**
     * After user is logged (verify is user is logged)
     * We will verify the permissions and resources
     *
     */
    $app->add(new app\middleware\ResourcePermission($app));
}



// Session Management
$app->container->singleton('storage', function () use ($redisClient) {
    return new \app\lib\storage\RedisStorage($redisClient);
});

$exceptionHandler = new \config\Exceptions($app);
$exceptionHandler->addExceptionHandlers();

$routeRequest = new \config\Router($app);

/**
 * Dependency injector class.
 *
 */
$app->container->singleton('di', function () use ($app) {
    return new \config\Dependencies($app);
});

$path_elements = explode('/', $app->request()->getPathInfo());
$controller = str_replace('/', '', $path_elements[1]);

if (strpos($app->request()->getPathInfo(), "/user") === 0 && $controller == 'user') {
    $routeRequest->resource('user');
} elseif (strpos($app->request()->getPathInfo(), "/campaign") === 0 && $controller == 'campaign') {
    $routeRequest->resource('campaign');
} elseif (strpos($app->request()->getPathInfo(), "/company") === 0 && $controller == 'company') {
    $routeRequest->resource('company');
} elseif (strpos($app->request()->getPathInfo(), "/login") === 0 && $controller == 'login') {
    $routeRequest->resource('login');
} elseif (strpos($app->request()->getPathInfo(), "/logout") === 0 && $controller == 'logout') {
    $routeRequest->resource('logout');
} else {
    exit('bad request');
}

$app->run();
